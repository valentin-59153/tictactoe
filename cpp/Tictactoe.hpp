#ifndef TICTACTOE_HPP
#define TICTACTOE_HPP

#include <array>
#include <iostream>

enum Joueur { JOUEUR_VIDE, JOUEUR_ROUGE, JOUEUR_VERT, JOUEUR_EGALITE };
/// \brief classe princiaple pour le tictactoe
///
///
///
class Jeu {
    private:
        // TODO
		std::array<std::array<Joueur, 3>,3> _plateau;
		Joueur joueur_courant=JOUEUR_ROUGE;
    public:
        /// \brief Constructeur à utiliser.
        ///
        Jeu();

		/// \brief Vainqueur	
        /// \return le vainqueur (ROUGE, VERT, EGALITE) ou VIDE si partie en cours.
        Joueur getVainqueur() const;

        // Retourne le joueur (ROUGE ou VERT) qui doit jouer.
        /// \brief joueur courant
        /// \return le joueur courant
        Joueur getJoueurCourant() const;

        // Joue un coup pour le joueur courant.
        // 
        // i ligne choisie (0, 1 ou 2)
        // j colonne choisie (0, 1 ou 2)
        // 
        // Si le coup est invalide, retourne false. Si le coup est valide,
        // joue le coup et retourne true.
        /// \brief Joue un coup
        /// bla bla bla bla les explications
        
        bool jouer(int i, int j);

        // Réinitialise le jeu.
        void raz();
        ///TU
        Joueur getPlateau(int i,int j) const;

        friend std::ostream & operator<<(std::ostream & os, const Jeu & jeu);
};

std::ostream & operator<<(std::ostream & os, const Jeu & jeu);

#endif

