#include "Tictactoe.hpp"

Jeu::Jeu() {
    raz();
}

void Jeu::raz() {
    for( auto & l : _plateau)
		for (auto & c : l )
			c=JOUEUR_VIDE;
}
Joueur Jeu::getPlateau(int i,int j) const{
	return _plateau[i][j];
}	

std::ostream & operator<<(std::ostream & os, const Jeu & jeu) {
for(const auto & l : jeu._plateau){
	for(const auto & c : l ) {
		if(c==JOUEUR_ROUGE) os <<"R";
			else if( c == JOUEUR_VERT) os <<"V";
			else os <<".";
		}
		os << std::endl;
	}
	return os;
}

Joueur Jeu::getVainqueur() const {
    // TODO
    return JOUEUR_VIDE;
}

Joueur Jeu::getJoueurCourant() const {
    return joueur_courant;
}

bool Jeu::jouer(int i, int j) {
    if(_plateau[i][j]==JOUEUR_VERT || _plateau[i][j]==JOUEUR_ROUGE) return false;
    else {
		_plateau[i][j]=getJoueurCourant();
		if(getJoueurCourant()==JOUEUR_ROUGE)joueur_courant=JOUEUR_VERT;
		else joueur_courant=JOUEUR_ROUGE;
		return true;
	}	
    
}

