#include "Tictactoe.hpp"
#include <sstream>

#include <CppUTest/CommandLineTestRunner.h>

void testerJeu(const std::string & str, const Jeu & jeu) {
    std::stringstream oss;
    oss << jeu;
    CHECK_EQUAL(str, oss.str());
};

TEST_GROUP(GroupTictactoe) { };

TEST(GroupTictactoe,test_Jeu) { 
	Jeu jeu;
	for(int i=0;i<3;i++){
		for(int j=0;j<3;j++){
			CHECK_EQUAL(JOUEUR_VIDE,jeu.getPlateau(i,j));
		}	
	}	
}

TEST(GroupTictactoe, test_affichage) { 
    Jeu jeu;
    testerJeu("...\n...\n...\n",jeu);
}

TEST(GroupTictactoe,test_jouer1){
	Jeu jeu;
	jeu.jouer(1,1);
	jeu.jouer(1,1);
	jeu.jouer(2,2);
	CHECK_EQUAL(JOUEUR_ROUGE,jeu.getPlateau(1,1));
	CHECK_EQUAL(JOUEUR_VERT,jeu.getPlateau(2,2));
}		

// TODO

